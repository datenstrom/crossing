#!/usr/bin/python3
#
#   -*- coding: utf-8 -*-
#
#   state.py
#   15.9.2016
#
#   A class to represent a state of the bridge and torch problem

from __future__ import division, print_function, absolute_import

import logging

__author__ = "Derek Goddeau"

_logger = logging.getLogger(__name__)


class State():
    """ A class to represent a single state of the bridge problem

    """
    def __init__(self, left=None, right=None, cost=0, torch_left=True):
        """ Initialize the state as empty

        """
        self.left = left
        self.right = right
        self.cost = cost
        self.torch_left = torch_left
        self.goal = self.is_goal()

    def __eq__(self, other):
        """ Compare two states for equality

        For uniform sorting purposes this will only check cost to determine
        if the states are equal. This should not be used to check for identical
        positioning of people.
        """
        return self.cost == other.cost

    def __lt__(self, other):
        """ Compare two states for less than using cost

        """
        return self.cost < other.cost

    def __le__(self, other):
        """ Compare two states for less than or equal using cost

        """
        return self.cost <= other.cost

    def __gt__(self, other):
        """ Compare two states for greater than using cost

        """
        return self.cost > other.cost

    def __gt__(self, other):
        """ Compare two states for greater than or equal using cost

        """
        return self.cost >= other.cost

    def get_moves(self):
        """ Move a person to the other side of the bridge and back.

        We ignore intermediate states where the torch is on the right side
        unless it is the final move of the last two people. These states
        can be traded to save memory in exchange for more computation
        up front.

        """
        if self.left == []:
            moves = []
        elif len(self.left) > 2 or not self.torch_left:

            tl = list(self.left)
            tr = list(self.right)
            new_cost = self.cost
            moves = []

            if self.torch_left:

                i = 0
                while i < len(self.left):

                    _logger.debug("---------------------")
                    _logger.debug("|      person       |")
                    _logger.debug("---------------------")
                    _logger.debug("initial tl: {}".format(tl))
                    _logger.debug("initial tr: {}".format(tr))

                    person1 = tl[i]
                    tr.append(person1)
                    tl.remove(person1)

                    tl_intermediate = list(tl)
                    tr_intermediate = list(tr)

                    j = 0
                    while j < len(tl):

                        person2 = tl[j]
                        tr.append(person2)
                        tl.remove(person2)

                        new_cost += max(person1, person2)

                        _logger.debug("right move tl: {}".format(tl))
                        _logger.debug("right move tr: {}".format(tr))
                        _logger.debug("cost: {}".format(new_cost))

                        new_state = State(tl, tr, new_cost, False)
                        moves.append(new_state)

                        tl = list(tl_intermediate)
                        tr = list(tr_intermediate)
                        new_cost = self.cost
                        j += 1

                    tl = list(self.left)
                    tr = list(self.right)
                    i += 1

            else:

                i = 0
                while i < len(self.right):

                    _logger.debug("---------------------")
                    _logger.debug("|      person       |")
                    _logger.debug("---------------------")
                    _logger.debug("initial tl: {}".format(tl))
                    _logger.debug("initial tr: {}".format(tr))

                    person = tr[i]
                    tr.remove(person)
                    tl.append(person)

                    new_cost += person

                    _logger.debug("left move tl: {}".format(tl))
                    _logger.debug("left move tr: {}".format(tr))
                    _logger.debug("cost: {}".format(new_cost))

                    new_state = State(tl, tr, new_cost, True)
                    moves.append(new_state)

                    tl = list(self.left)
                    tr = list(self.right)
                    new_cost = self.cost
                    i += 1

        elif(len(self.left) == 2 and self.torch_left):

            moves = []
            new_cost = self.cost + max(self.left[0], self.left[1])

            right = self.left + self.right

            new_state = State([], right, new_cost, False)
            moves.append(new_state)

        elif(len(self.left) == 1 and self.torch_left):

            moves = []
            new_cost = self.cost + self.left[0]

            right = self.left + self.right

            new_state = State([], right, new_cost, False)
            moves.append(new_state)

        else:

            raise ValueError('Input parameters incorrect')

        return moves

    def is_goal(self):
        """ Return True if state is a goal state

        """
        if self.left is []:
            return True
