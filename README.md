The Crossing
============

Four people want to get to the other side of a river by crossing a very narrow
tunnel that a maximum of two people may cross at a time. It is nighttime, so
someone must carry the single flashlight during each crossing. One of them
takes 1 minute to cross the tunnel, another takes 2 minutes, another takes 5
minutes, and the last takes 10 minutes to cross. Then, we name them persons 1,
2, 5, and 10, respectively. If two people cross the tunnel together, they must
walk at the pace of the slower one. So, if persons 5 and 10 cross together,
they take 10 minutes to get to the other side. (If person 5 crosses by himself,
it takes 5 minutes.) How long does it take all four people to get to the other
side? Of course, it depends on how they do it. From the initial state (1, 2, 5,
and 10 on the same side with the flashlight) there are ten possible crossing
actions: 1 crosses alone, 2 crosses alone, 5 crosses alone, 10 crosses alone, 1
and 2 cross, 1 and 5 cross, 1 and 10 cross, 2 and 5 cross, 2 and 10 cross, or 5
and 10 cross. (If two people cross, it doesn't matter which one holds the
flashlight.)

Here is an example solution:

    1 and 10 go across 10 minutes
    1 goes back with light 1 minute
    1 and 5 go across 5 minutes
    1 goes back with light 1 minute
    1 and 2 go across 2 minutes
    Total 19 minutes

For this problem, 19 minutes is a solution, this is not the
fastest solution.

Analysis
--------

See the Jupyter Notebook: [https://datenstrom.gitlab.io/crossing](datenstrom.gitlab.io/crossing)

Program Usage
-------------

The program is implemented in Python3 and will not run on earlier versions,
to run no compilation is required just a Python3 installation no dependencies
from pip are required. It uses a command line interface to accept input
which determines the algorithm to use and also the representation of each
person. The command line interface provides help messages when incorrect
formatting is used or the `-h` help flag is used.

    user@host$ python3 bridge.py -h
    usage: bridge.py [-h] [-d | -b | -u] speeds [speeds ...]

    Example search algorithm implementation

    positional arguments:
      speeds         List of speeds representing people

    optional arguments:
      -h, --help     show this help message and exit
      -d, --depth    Use the depth first search algorithm
      -b, --breadth  Use the breadth first search algorithm
      -u, --uniform  Use the uniform search algorithm

The first argument is the flag choosing the algorithm to use and these options
are mutually exclusive. The second argument is a list of integers representing
a persons speed.


### Depth First Search Example

    user@host$ python3 bridge.py -d 1 2 5 10
    The cost is: 24

### Breadth First Search Example

    user@host$ python3 bridge.py -b 1 2 5 10
    The cost is: 17

### Uniform-Cost Search Example

    user@host$ python3 bridge.py -u 1 2 5 10
    The cost is: 17

### Debugging

The debug log is output to `search.log`.
