#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#   search.py
#   15.9.2016
#
#   Depth first search implementation

from __future__ import division, print_function, absolute_import

import logging
import queue

from state import State

__author__ = "Derek Goddeau"

_logger = logging.getLogger(__name__)


def dfs(speeds):
    """ Depth first search

    """
    state = State(speeds, [], 0, True)
    fringe = queue.LifoQueue()
    fringe.put(state)

    _logger.debug("Initial State: {}".format(state.left))

    while not state.is_goal():
        state = fringe.get()
        _logger.debug("============ Poped state: {} ==============".format(state.left))

        moves = state.get_moves()
        _logger.debug("------- Move List --------")
        for move in moves:
            _logger.debug("left: {}   right: {}   cost: {}".format(move.left, move.right, move.cost))
            fringe.put(move)

        if moves == []:
            break

        _logger.debug("--------------------------")
        
    return state.cost


def bfs(speeds):
    """ Breadth first search

    """
    state = State(speeds, [], 0, True)
    fringe = queue.Queue()
    fringe.put(state)
    optimal = float("inf")

    _logger.debug("Initial State: {}".format(state.left))

    while not fringe.empty():

        state = fringe.get()



        _logger.debug("============ Poped state: {} ==============".format(state.left))

        moves = state.get_moves()
        _logger.debug("------- Move List --------")
        
        for move in moves:
            _logger.debug("left: {}   right: {}   cost: {}".format(move.left, move.right, move.cost))
            fringe.put(move)

        _logger.debug("--------------------------")

        if state.cost < optimal and state.left == []:
            return state.cost



def uniform(speeds):
    """ uniform search

    """
    state = State(speeds, [], 0, True)
    fringe = queue.PriorityQueue()
    fringe.put(state)

    _logger.debug("Initial State: {}".format(state.left))

    while not state.is_goal():
        state = fringe.get()
        _logger.debug("============ Poped state: {} ==============".format(state.left))

        moves = state.get_moves()
        _logger.debug("------- Move List --------")
        for move in moves:
            _logger.debug("left: {}   right: {}   cost: {}".format(move.left, move.right, move.cost))
            fringe.put(move)

        if moves == []:
            break

        _logger.debug("--------------------------")
        
    return state.cost
