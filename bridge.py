#!/usr/bin/python3
# -*- coding: utf-8 -*-
""" bridge.py

This module runs the main program loop for the classical bridge torch proble. It
collects arguments to run with or supplies the default perameters, then prints
the solution to the problem. This may be done using a variety of search
algorithms.

"""

from __future__ import division, print_function, absolute_import

import argparse
import sys
import logging

import search

__author__ = "Derek Goddeau"

_logger = logging.getLogger(__name__)

def main(args):
    """ Main
    
    """
    setup_logs()
    _logger.debug("Starting main()")

    args = parse_args(args)
    if args.depth:
        cost = search.dfs(args.speeds)
        print("The cost is: {}".format(cost))
    elif args.breadth:
        cost = search.bfs(args.speeds)
        print("The cost is: {}".format(cost))
    elif args.uniform:
        cost = search.uniform(args.speeds)
        print("The cost is: {}".format(cost))
    else:
        main(['-h'])

    _logger.debug("All done, shutting down.")
    logging.shutdown()

def parse_args(args):
    """ Parse command line parameters.

    :return: command line parameters as :obj:`airgparse.Namespace`
    Args:
        args ([str]): List of strings representing the command line arguments.

    Returns:
        argparse.Namespace: Simple object with a readable string
        representation of the argument list.

    """
    parser = argparse.ArgumentParser(
        description="Example search algorithm implementation")
    algorithms = parser.add_mutually_exclusive_group()
    algorithms.add_argument(
        '-d',
        '--depth',
        help="Use the depth first search algorithm",
        action="store_true")
    algorithms.add_argument(
        '-b',
        '--breadth',
        help="Use the breadth first search algorithm",
        action="store_true")
    algorithms.add_argument(
        '-u',
        '--uniform',
        help="Use the uniform search algorithm",
        action="store_true")
    parser.add_argument(
        'speeds', 
        nargs='+',
        type=int,
        default=[1, 2, 5, 10],
        help="List of speeds representing people")
    return parser.parse_args(args)


def setup_logs():
    """ Set up logger to be used between all modules.

    Set logging root and file handler configuration to default to
    ``DEBUG`` and write output to ``search.log``. Set console
    handler to default to ``ERROR``.

    """
    logging.basicConfig(level=logging.DEBUG, filename='search.log',
                        filemode='w')
    _logger.setLevel(logging.DEBUG)

    # create file handler which logs messages
    fh = logging.FileHandler('search.log')
    fh.setLevel(logging.DEBUG)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    # add the handlers to the logger
    _logger.addHandler(fh)
    _logger.addHandler(ch)


if __name__ == "__main__":

    main(sys.argv[1:])
    sys.exit(0)
